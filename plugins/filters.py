def butlast(xs):
    return xs[:-1]

def fmt(x, format_str):
    return format_str % x

def split(x, delim):
    return x.split(delim)

class FilterModule(object):
    def filters(self):
        return {
            'butlast': butlast,
            'fmt'    : fmt,
            'split'  : split
        }

# eof
