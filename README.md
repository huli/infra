<img src="Logo.png" />

```
 _        __           
(_)_ __  / _|_ __ __ _ 
| | '_ \| |_| '__/ _` |
| | | | |  _| | | (_| |
|_|_| |_|_| |_|  \__,_|
                       
Portable, immutable cloud infrastructure.
```

infra v0.1.0
============
infra is a collection of Ansible scripts used to manage immutable 
servers across different cloud providers.

Roles are provided by a separate package, server, symlinked to ./roles.

It introduces some conventions for organizing Ansible data (the mapping
of roles to servers and variables needed by roles) and a protocol for
deploying changes atomically, without touching running instances.

Both aspects are described in detail below.

Usage
=====

  gmake $playbook infra=$path
  
Where $playbook is one of [bake|configure|deploy] and $path is the full
path to the infra directory WITHOUT A TRAILING SLASH.

Data
====
Since cloud deployments are characterized by a dynamic host set, we
depart somewhat from the usual Ansible best practices.
In particular, instead of the typical inventory file/playbook dir/
vars dir, each infrastructure (corresponding roughly to some related
servers running within a single account on a single cloud provider)
is represented by a directory containing:

infra
-----
* an infra.yml file, which must declare
  * the 'substrate' to use (a substrate is basically a cloud provider)
  * the variables used by that substrate's callbacks
  * a list of 'boxes' to instantiate
* a baseimage.yml file (a list of roles to include)
* one or more boxes; a $box is a directory containing:
  * a configure.yml script (a list of roles to include)
  * an up.yml script (called when the box is started)
  * a down.yml script (called when the box is stopped)
* a ssh.priv file containing the private key corresponding
  to substrate.credentials.{ssh, key_name}

Example
-------

    myinfra $ ls
    baseimage.yml infra.yml mysvc/ ssh.priv vault.pass
    
    myinfra $ ls mysvc/
    configure.yml down.yml up.yml
    
    myinfra $ cat infra.yml
    
    substrate: do
    
    do:
      credentials:
        api_token: "{{ vault_do_api_token }}"
      # ...

    boxes:
      -name: mysvc
       count: 1

    myinfra $ cat baseimage.yml
    - set_fact: myvar=...
    # ...
    
    - import_role: name=...
      vars:
        myvar2=...
    # ...

    myinfra $ cat mysvc/configure.yml
    - set_fact: ...
    - include_role: ...
    
    myinfra $ cat mysvc/up.yml
    # E.g. switch over Elastic IP
    
    myinfra $ cat mysvc/down.yml
    # Often empty.

Protocol
========
Servers are provisioned in three steps

1) We bake a base machine image using your infra's baseimage.yml file
   * this image should include as much as possible to speed up the next 
     step
   * you should only need to run this when you want to upgrade any of 
     the third-party packages used by your infra
2) We start fresh instances based on the base image and configure them
   * ideally only adding the latest version of your app code from S3 or
     similar
   * these instances are spun up separately from any currently running 
     instances
3) We deploy by switching over from the old instances atomically
   * this needs to be implemented by your up.yml/down.yml scripts, 
     typically these will talk to your load balancer or similar

Have a look at playbooks/ and deploy.yml in particular for details.
Some care has been taken to ensure that re-running these scripts
should converge to a cloud account containing a set of instances
named $infra-$box-$id (where $id is just a counter) which matches
the 'boxes' spec in your infra.yml.

Substrate
=========
A substrate is a cloud service provider. We currently support AWS and 
DigitalOcean, but adding additional providers is easy. 
Substrate-specific configuration data is stored in a variable with the
same name as the substrate. 
The required fields currently differ by substrate:

required by all
---------------
* privileges.remote_user
* privileges.become
* privileges.become_method
* user_data

aws
---
* credentials.access_key
* credentials.secret_key
* credentials.key_name
* distroimage.name
* distroimage.owner
* config.region
* config.instance_type
* config.security_group
* config.volume

do
--
* credentials.api_token
* credentials.ssh
* distroimage.distribution
* distroimage.name
* config.region
* config.size
* config.firewall

vultr
-----
* credentials.api_key
* config.plan
* config.region
* config.ssh_key
* config.firewall
* distroimage.family
* distroimage.name

callbacks
---------
To support a new substrate, the following scripts need to be implemented:

* images_base
* images_create
* images_distro
* instances_list
* instances_set_firewall
* instances_set_ip
* instances_set_name
* instances_start
* instances_stop
* instances_terminate

Coding Conventions
==================
We make heavy use of Ansible's include-based subroutines.
Each routine takes arguments prefixed with in_ (always passed
explicitly) and returns its result in a variable with the same name as 
the script file. Inspecting tasks/ should make this clear.

All files must end with "# eof" on a separate line (because YAML is not
truncation-safe).

TODO
====
* important parts currently handled outside of Ansible:
  * EIP/DNS mapping (service discovery)
  * security groups (intra instance routing)
    * we could generate a new secgroup for each instance at configure 
      time and add secgroups to eachother's inbound rulesets as
      specified in a routing table in infra.yml
* explain include/import semantics and safety
* type annotations
* more support for stateful services (DB)

<3
